# Task 5

## Mockup

Open the [this mockup](https://xd.adobe.com/view/34197391-c463-4bb0-b8b0-8b6f56f0d2d4-614a/specs/) in browser. Page 1 is desktop view and page 2 is mobile view. All asset files are downloadable from that link. All color codes are available from the link also.

## Actual Task

- Implement the mockups as a responsive HTML page.
- Use any libraries as you see fit.
- All required font files are located in `fonts` directory.
- Contact form submission requires API integration, thus optional.
- All animations require Lottie integration, you may use the static images from the mockup instead. If you are up for it though, the files are located in `lottie` directory.
- Write **CLEAN** and **WELL-STRUCTURED** HTML and CSS. This means all nested elements are indented properly.