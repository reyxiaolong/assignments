// <--- prints FibNumber --->

/* as a reference [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]; 
enter 11 as parameter to print between 1 to 100 */

// var initial = f0 = 0, f1 = 1
// var recurrence  = Fn = F(n-1) + F(n-2)
function fibPrinter(n) {
    var output = [];
    if (n === 1) {
        output = [1];
    } else if (n === 2) {
        output = [1, 1];
    } else {
        output = [1, 1];
        for (var i = 2; i < n; i++) {
            output.push(output[output.length - 2] + output[output.length - 1]);
    }
        }
return output;  
}

output = fibPrinter(11);
console.log(output);

// <--- prints primeNumber --->

/* as a reference [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]  25 items */

function isPrime(num) {
    for (var i = 2; i < num; i++) { 
        if (num % i === 0){
        	return false;
        }
}
return true;
}

function primePrinter(n) {
    var prime = [2];
        for (var i = 3; i < n; i+=2) {
            if (isPrime(i)) {
                prime.push(i);
        	}
		}
return prime;
} 

output2 = primePrinter(100);
console.log(output2);

// Given the following number sequence: 1, 6, 15, 28, 45, 66, ...
// Write a function that accepts 2 input argument: start and end.
// The function must print number within the above sequence
// between start(inclusive) and end(inclusive).

// For example: printSequence(6, 70) will produce: ...| 6, 15, 28, 45, 66 |... 
// ...91, 120, 153, 190, 231, {12}276, 325, 378, 435, 496, 561, 630, 703, 780, {21}861, 946 
    


function seq1(x) {
    return x * (2 * x - 1);
}

function printSequence(start, end) {
    
    let list = [];
    
    for (var i = 1; i <= end; i++) {             
        list.push(seq1(i));    
        if (seq1(i) > end) {                
            list.pop();
        }      
    }

    for (var i = 1; i <= end; i++) {
        if (seq1(i) < start) {
            list.shift();
        }
    }
    console.log(list);
    return list;
}


// #1. update your fibonacci and prime number function to accept start and end, then print only the number within that range (start and end inclusive)
/* fibonacci = [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]; */

function fibF2(y) {
    return ( Math.round((((1 + Math.sqrt(5)) / 2 ) **y) / Math.sqrt(5)) );
}

function printFib(start, end) {
    var fib = [];

            for (var i = 1; i <= end; i++) {
                fib.push(fibF2(i));
                if (fibF2(i) > end) {
                    fib.pop();
                }
            }
            
            for (var i = 1; i <= end; i++) {
                if (fibF2(i) < start) {
                    fib.shift();
                }
            }
    return fib;   
}

/* prime = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97] */

function isPrime(num) {
    if (num === 1 || num === 0) {
        return false;
    }
    for (var i = 2; i < num; i++) { 
        if (num % i === 0) {
        	return false;
        }
    }
    return true;
}

function printPrime(start, end) {
    var primeIn = "";

    for (var i = start; i <= end; i++) {        
        if (isPrime(i)) {
            primeIn += i + ", ";
        }
    }

    console.log(primeIn);
    return primeIn;
}

// #2. create a function that checks if the input argument is a palindrome

function palindrome(lol) {
    var txt = lol.split("").reverse().join("");

    if (lol == txt) {
        console.log(`${lol} is a palindrome`)
    } else {
        console.log(`${lol} is not a palindrome`)
    }
 
}

// #3. create a function that check if the first argument is an anagram of the the second argument

function anagram(first, second) {

    var x = first.split("").sort().join("");
    var y = second.split("").sort().join("");

    if (x === y) {
        console.log(`${first} is an anagram of ${second}.`);
    } else {
        console.log(`${first} is not an anagram of ${second}.`)
    }
}

